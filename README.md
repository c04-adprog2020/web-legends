# Web Legends

master: [![pipeline status](https://gitlab.com/c04-adprog2020/web-legends/badges/master/pipeline.svg)](https://gitlab.com/c04-adprog2020/web-legends/-/commits/master) [![coverage report](https://gitlab.com/c04-adprog2020/web-legends/badges/master/coverage.svg)](https://gitlab.com/c04-adprog2020/web-legends/-/commits/master)

Krisna (Item): [![pipeline status](https://gitlab.com/c04-adprog2020/web-legends/badges/item/pipeline.svg)](https://gitlab.com/c04-adprog2020/web-legends/-/commits/item) [![coverage report](https://gitlab.com/c04-adprog2020/web-legends/badges/item/coverage.svg)](https://gitlab.com/c04-adprog2020/web-legends/-/commits/item)

Tama (combatLogic): [![pipeline status](https://gitlab.com/c04-adprog2020/web-legends/badges/combatLogic/pipeline.svg)](https://gitlab.com/c04-adprog2020/web-legends/-/commits/combatLogic) [![coverage report](https://gitlab.com/c04-adprog2020/web-legends/badges/combatLogic/coverage.svg)](https://gitlab.com/c04-adprog2020/web-legends/-/commits/combatLogic)

Yoga (frontend & auth): [![pipeline status](https://gitlab.com/c04-adprog2020/web-legends/badges/frontend/pipeline.svg)](https://gitlab.com/c04-adprog2020/web-legends/-/commits/frontend) [![coverage report](https://gitlab.com/c04-adprog2020/web-legends/badges/frontend/coverage.svg)](https://gitlab.com/c04-adprog2020/web-legends/-/commits/frontend)
[![pipeline status](https://gitlab.com/c04-adprog2020/web-legends/badges/authentication/pipeline.svg)](https://gitlab.com/c04-adprog2020/web-legends/-/commits/authentication) [![coverage report](https://gitlab.com/c04-adprog2020/web-legends/badges/authentication/coverage.svg)](https://gitlab.com/c04-adprog2020/web-legends/-/commits/authentication)

Pashya (Character): [![pipeline status](https://gitlab.com/c04-adprog2020/web-legends/badges/characterfinal/pipeline.svg)](https://gitlab.com/c04-adprog2020/web-legends/-/commits/characterfinal) [![coverage report](https://gitlab.com/c04-adprog2020/web-legends/badges/characterfinal/coverage.svg)](https://gitlab.com/c04-adprog2020/web-legends/-/commits/characterfinal)

Sean(Backend): [![pipeline status](https://gitlab.com/c04-adprog2020/web-legends/badges/Player/pipeline.svg)](https://gitlab.com/c04-adprog2020/web-legends/-/commits/Player) [![coverage report](https://gitlab.com/c04-adprog2020/web-legends/badges/Player/coverage.svg)](https://gitlab.com/c04-adprog2020/web-legends/-/commits/Player)


Web Legends adalah sebuah game online singleplayer dengan genre turn-based RPG. Game ini dibuat oleh kelompok AP C-04 dengan pembagian sebagai berikut:

1. I Made Krisna Dwitama (Combat Logic & Enemy Creation)
2. Mahardika Krisna Ihsani (Item & Powerup)
3. Ahmad Yoga Haulian Prtama (Game Stage & Flow, Frontend)
4. Sean Zheliq Urian (Scoring, Backend)
5. Muhammad Adipasha Yarzuq (Character Creation)

## Fitur-Fitur:

- Random item drop setiap kali pemain berhasil mengalahkan musuh
- Character creation (Menu untuk membuat karakter player yang disajikan sebelum pemain masuk ke stage awal )
- HP untuk pemain dan musuh serta MP(Mana Point) untuk player. MP digunakan untuk mengaktifkan skill.
- Terdapat waktu cooldown yang bisa berbeda untuk setiap skill 
-  Global high score
-  Boss fight pada akhir stage
-  Powerup setiap kali pemain berhasil mengalahkan musuh

## Task Checklist:
- [x] Membuat item
- [x] Implementasi random drop
- [x] Implementasi powerup
- [x] Handling apabila item tidak termasuk
- [ ] Refactor kodingan 
- [x] Membuat skill character
- [ ] Implementasi AI
- [ ] Membuat class character untuk enemy
- [ ] Frontend webpage
- [ ] Membuat gamestage 
- [ ] Membuat menu 
- [ ] Membuat class character
- [ ] Implementasi menu character creation
- [ ] Membuat class weapon
- [ ] Membuat character equipment
- [ ] Setup database
- [ ] Implementasi game session
- [ ] Implementasi game manager untuk me-retrieve high score dari database  

