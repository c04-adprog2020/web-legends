
package com.c04.weblegends.service;

import com.c04.weblegends.service.*;
import com.c04.weblegends.core.character.*;
import com.c04.weblegends.core.aicharacter.*;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class EnemyServiceTest {
    EnemyService enemyService;
    @BeforeEach
    public void setUp(){
        enemyService=new EnemyServiceImpl();
        enemyService.spawnEnemy(1);
    }
    @Test
    public void getterTest(){
        assertTrue(enemyService.getEnemy() instanceof ArtificialIntelligence);
        assertNotNull(enemyService.getEnemy());
        assertTrue(enemyService.getDamage() > 0);
        assertTrue(enemyService.getHP() > 0);
        assertFalse(enemyService.isDead());
    }
    @Test
    public void setterTest(){
        enemyService.setHP(0);
        assertTrue(enemyService.isDead());
    }

}
