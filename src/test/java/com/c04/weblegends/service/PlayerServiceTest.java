
package com.c04.weblegends.service;

import com.c04.weblegends.repo.PlayerRepo;
import com.c04.weblegends.service.*;
import com.c04.weblegends.core.character.*;
import com.c04.weblegends.core.item.*;
import com.c04.weblegends.core.weapon.*;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class PlayerServiceTest {
    PlayerService playerService;
    @BeforeEach
    public void setUp(){
        this.playerService=new PlayerServiceImpl();
    }

    @Test
    public void testRandomItem() {
        ArrayList <Item> itemList=new ArrayList<Item>();
        Item item=playerService.getItem();
        itemList.add(item);
        assertTrue(item instanceof Item);
        item=playerService.getItem();
        itemList.add(item);
        item=playerService.getItem();
        itemList.add(item);
        item=playerService.getItem();
        itemList.add(item);
        assertEquals(itemList.size(),playerService.getItems().size());
        assertEquals(4,playerService.getItems().size());
        playerService.useItem(0);
        assertEquals(3,playerService.getItems().size());
    }

    @Test
    public void testRandomPowerup() {
        assertEquals("Character successfully upgraded!",playerService.doPowerUp());
        assertNotNull(playerService.getHero());
        assertTrue(playerService.getHero() instanceof Powerup);
    }

    @Test
    public void testRandomEnhancement() {
        assertEquals("Weapon successfully upgraded!",playerService.doEnhanceWeapon());
        assertNotNull(playerService.getWeapon());
        assertTrue(playerService.getWeapon() instanceof Enhancement);
        assertEquals(playerService.getHero().getWeapon(),playerService.getWeapon());
    }

    @Test
    public void dummyTest(){
        playerService.changeObject();
        assertEquals(1,1);
    }

    @Test
    public void testSetter(){
        assertEquals(3000,playerService.getHP());
        playerService.setHP(2000);
        assertEquals(2000,playerService.getHP());
        assertEquals(10,playerService.getDamage());
        assertTrue(playerService.getRepo() instanceof PlayerRepo);
    }

    @Test
    public void createHeroTest(){
        playerService.createHero("w","knight");
        assertTrue(playerService.getHero() instanceof  Knight);
        assertTrue(playerService.getWeapon() instanceof Sword);
        assertEquals("w",playerService.getHero().getName());
    }

    @Test
    public void setterTest(){
        playerService.setHP(0);
        assertTrue(playerService.isGameOver());
    }


}

