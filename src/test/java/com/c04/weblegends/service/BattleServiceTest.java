
package com.c04.weblegends.service;

import com.c04.weblegends.service.*;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class BattleServiceTest {
    BattleService battleService;
    @BeforeEach
    public void setUp(){
        PlayerService playerService = new PlayerServiceImpl();
        battleService=new BattleServiceImpl(playerService);
    }
    @Test
    public void changeStage(){
        battleService.changeStage();
        assertEquals(1,battleService.getLevel());
    }
    @Test
    public void getterTest(){
        assertTrue(battleService.getEnemy() instanceof  EnemyService);
        assertTrue(battleService.getPlayer() instanceof  PlayerService);
    }
    @Test
    public void playerAttack(){
        battleService.changeStage();
        assertNotNull(battleService.playerAttack());
    }
    @Test
    public void enemyAttack(){
        battleService.changeStage();
        assertNotNull(battleService.enemyAttack());
    }
    @Test
    public void playerSpell(){
        battleService.changeStage();
        assertNotNull(battleService.playerSpell());
    }
    @Test
    public void changeObject(){
        battleService.changeObject();
        assertEquals(0,0);
    }

}
