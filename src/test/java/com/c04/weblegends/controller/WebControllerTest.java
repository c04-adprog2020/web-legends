
package com.c04.weblegends.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.c04.weblegends.controller.*;
import com.c04.weblegends.core.item.Item;
import com.c04.weblegends.service.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;

@WebMvcTest(controllers = WebController.class)
public class WebControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PlayerService playerService;
    
    @Test
    public void testHome() throws  Exception {
        mockMvc.perform(get("/")).andExpect(status().isOk());
    }

    @Test
    public void toCharacter() throws  Exception{
        mockMvc.perform(get("/tochar"))
                .andExpect(redirectedUrl("/character/save"));
    }
}
