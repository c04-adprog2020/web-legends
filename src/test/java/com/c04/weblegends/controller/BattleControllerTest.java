
package com.c04.weblegends.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.c04.weblegends.controller.*;
import com.c04.weblegends.core.item.Item;
import com.c04.weblegends.service.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;

@WebMvcTest(controllers = {ItemController.class,BattleController.class})
public class BattleControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BattleService battleService;

    @Test
    public void testSave() throws Exception {
        PlayerService playerService=new PlayerServiceImpl();
        playerService.getItem();
        battleService=new BattleServiceImpl(playerService);
        mockMvc.perform(get("/battle/save/").flashAttr("service",battleService)).andExpect(redirectedUrl("/battle/"));
    }

    @Test
    public void testPost() throws Exception {
        PlayerService playerService=new PlayerServiceImpl();
        playerService.getItem();
        battleService=new BattleServiceImpl(playerService);
        mockMvc.perform(get("/battle/save/").flashAttr("service",battleService))
                .andExpect(redirectedUrl("/battle/")).andDo(
                        result -> mockMvc.perform(post("/battle/use/")
                                .param("itemIndex","0")).andExpect(redirectedUrl("/battle/")));
    }

    @Test
    public void testRedirectToItem() throws  Exception{
        mockMvc.perform(get("/battle/item/")).andExpect(redirectedUrl("/item/save"));
    }

    @Test
    public void battleScreenTest() throws Exception{
        mockMvc.perform(get("/battle/")).andExpect(status().isOk());
    }

    @Test
    public void attackTest() throws Exception{
        PlayerService playerService=new PlayerServiceImpl();
        playerService.getItem();
        battleService=new BattleServiceImpl(playerService);
        mockMvc.perform(get("/battle/save/").flashAttr("service",battleService))
                .andExpect(redirectedUrl("/battle/")).andDo(
                result -> mockMvc.perform(get("/battle/attack/"))
            .andExpect(status().isOk()));

    }

    @Test
    public void skillTest() throws Exception{
        PlayerService playerService=new PlayerServiceImpl();
        playerService.getItem();
        battleService=new BattleServiceImpl(playerService);
        mockMvc.perform(get("/battle/save/").flashAttr("service",battleService))
                .andExpect(redirectedUrl("/battle/")).andDo(
                result -> mockMvc.perform(get("/battle/skill/"))
                        .andExpect(status().isOk()));

    }

    @Test
    public void spellTest() throws Exception{
        PlayerService playerService = new PlayerServiceImpl();
        playerService.getItem();
        battleService = new BattleServiceImpl(playerService);
    }
}
