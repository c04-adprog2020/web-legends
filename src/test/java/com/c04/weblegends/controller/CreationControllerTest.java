
package com.c04.weblegends.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.c04.weblegends.controller.*;
import com.c04.weblegends.core.item.Item;
import com.c04.weblegends.service.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Arrays;
import java.util.List;

@WebMvcTest(controllers = {WebController.class,CreationController.class})
public class CreationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PlayerService playerService;


    @Test
    public void testPost() throws Exception {
        PlayerService playerService=new PlayerServiceImpl();
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("heroName", "w");
        params.add("heroClass","knight");
        mockMvc.perform(get("/character/save/").flashAttr("service",playerService))
                .andExpect(redirectedUrl("/character/")).andDo(
         result -> mockMvc.perform(post("/character/produce/").params(params))
                .andExpect(redirectedUrl("/character/")) );
    }

    @Test
    public void testHome() throws  Exception {
        PlayerService playerService=new PlayerServiceImpl();
        mockMvc.perform(get("/character/save/").flashAttr("service",playerService))
                .andExpect(redirectedUrl("/character/")).andDo(
         result -> mockMvc.perform(get("/character/")).andExpect(status().isOk())
        );
    }

    @Test

    public void testToBattle() throws  Exception {
        mockMvc.perform(get("/character/save/").flashAttr("service",playerService))
                .andExpect(redirectedUrl("/character/")).andDo(
        result -> mockMvc.perform(get("/character/battle/")).andExpect(redirectedUrl("/battle/save"))
        );
    }

    @Test
    public void testReceiver() throws  Exception{
        PlayerService playerService = new PlayerServiceImpl();
        mockMvc.perform(get("/character/save/").flashAttr("service",playerService)).andExpect(redirectedUrl("/character/"));
    }
}

