
package com.c04.weblegends.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import com.c04.weblegends.controller.*;
import com.c04.weblegends.core.item.Item;
import com.c04.weblegends.service.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = ItemController.class)
public class ItemControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BattleService battleService;


    @Test
    public void testSave() throws Exception {
        PlayerService playerService=new PlayerServiceImpl();
        playerService.getItem();
        battleService=new BattleServiceImpl(playerService);
        mockMvc.perform(get("/item/save/").flashAttr("service",battleService)).andExpect(redirectedUrl("/battle/save"));
    }
}
