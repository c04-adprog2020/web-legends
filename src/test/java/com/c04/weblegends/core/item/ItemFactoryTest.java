package com.c04.weblegends.core.item;

import com.c04.weblegends.core.character.*;
import com.c04.weblegends.core.item.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.*;


public class ItemFactoryTest {
    ItemFactory itemFactory;
    CharComponent character;

    @BeforeEach
    public void setUp() {
        character=new Saber("Apollonia");
        itemFactory = new ItemFactory(character);
    }

    @Test
    public void createItemTest() {
        Item item = itemFactory.createItem();
        assertTrue(item instanceof Item);
        assertNotNull(item);
        assertEquals(character,item.getPlayer());
    }

    @Test
    public void changePlayerTest(){
        assertEquals(character,itemFactory.getPlayer());
        CharComponent chara=new Saber("arthur");
        itemFactory.setCharacter(chara);
        assertEquals(chara,itemFactory.getPlayer());
    }


}
