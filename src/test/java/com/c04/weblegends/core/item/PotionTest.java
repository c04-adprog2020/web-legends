package com.c04.weblegends.core.item;

import com.c04.weblegends.core.character.*;
import com.c04.weblegends.core.item.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNull;

public class PotionTest {
    CharComponent character;
    Item item;

    @BeforeEach
    public void setUp() {
        character = new Saber("Apollonia");
        item = new Potion(character, 200);
        character.setHP(100);
    }

    @Test
    public void useTest() {
        item.use();
        assertEquals(300, character.getHP());
    }

    @Test
    void getterTest() {
        assertEquals(200, item.getHP());
        assertEquals(0, item.getMana());
        assertEquals("potion", item.getType());
        assertEquals(character,item.getPlayer());
    }

    @Test
    void setterTest(){
        CharComponent chara=new Saber("arthur");
        item.setPlayer(chara);
        assertEquals(chara,item.getPlayer());
    }

}
