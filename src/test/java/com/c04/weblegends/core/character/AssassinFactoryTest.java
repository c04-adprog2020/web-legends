package com.c04.weblegends.core.character;

import com.c04.weblegends.core.character.*;
import com.c04.weblegends.core.weapon.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class AssassinFactoryTest {
    CharFactory assassinFactory;
    @BeforeEach
    public void setUp(){
        assassinFactory=new AssassinFactory();
    }
    @Test
    public void getterTest(){
        assertEquals("assassin",assassinFactory.getType());
    }
    @Test
    public void produceTest(){
        CharComponent chara=assassinFactory.produce("w");
        assertEquals("assassin",chara.getClassName());
        assertEquals("w",chara.getName());
        assertEquals("backstab",chara.getSpell().getName());
        assertNotNull(chara);
        assertTrue(chara.getWeapon() instanceof  Dagger);
    }

}
