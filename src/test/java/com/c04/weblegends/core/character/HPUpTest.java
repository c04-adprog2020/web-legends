package com.c04.weblegends.core.character;

import com.c04.weblegends.core.character.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNull;

public class HPUpTest {
    CharComponent arthur;

    @BeforeEach
    public void setUp() {
        arthur = new Saber("Apollonia");
        arthur = new HpUp(arthur, 10);
    }


    @Test
    public void testGetter() {
        assertEquals(3300, arthur.getMaxHP());
        assertEquals(500, arthur.getMaxMana());
        assertEquals("Apollonia", arthur.getName());
        assertEquals("Saber", arthur.getClassName());


    }

    @Test
    public void testSetter() {
        arthur.setHP(100);
        arthur.setMana(40);
        assertEquals(100, arthur.getHP());
        assertEquals(40, arthur.getMana());
        arthur.setHP(-2);
        arthur.setMana(-3);
        assertEquals(0, arthur.getHP());
        assertEquals(0, arthur.getMana());
        arthur.setHP(10000);
        arthur.setMana(10000);
        assertEquals(3300, arthur.getHP());
        assertEquals(500, arthur.getMana());
    }


}

