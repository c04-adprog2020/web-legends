package com.c04.weblegends.core.character;

import com.c04.weblegends.core.character.*;
import com.c04.weblegends.core.weapon.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNull;

public class CharacterTest {
    Character arthur;

    @BeforeEach
    public void setUp() {
        arthur = new Saber("Apollonia");
    }

    @Test
    public void testGetter() {
        assertEquals(3000, arthur.getMaxHP());
        assertEquals(500, arthur.getMaxMana());
        assertEquals("Apollonia", arthur.getName());
        assertEquals("Saber", arthur.getClassName());
        assertEquals("slash", arthur.getSpell().getName());
    }

    @Test
    public void testSetter() {
        arthur.setMana(40);
        arthur.setHP(100);
        assertEquals(100, arthur.getHP());
        assertEquals(40, arthur.getMana());
        arthur.setHP(10000);
        arthur.setMana(10000);
        assertEquals(3000, arthur.getHP());
        assertEquals(500, arthur.getMana());
        arthur.setHP(-2);
        arthur.setMana(-3);
        assertEquals(0, arthur.getHP());
        assertEquals(0, arthur.getMana());

    }
}
