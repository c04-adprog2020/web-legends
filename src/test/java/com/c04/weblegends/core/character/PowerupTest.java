package com.c04.weblegends.core.character;

import com.c04.weblegends.core.character.*;
import com.c04.weblegends.core.weapon.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNull;

public class PowerupTest {
    CharComponent arthur, arthur2, arthur3, arthur4;
    WeaponComponent weapon;


    @BeforeEach
    public void setUp() {
        arthur = new Saber("Apollonia");
        weapon=new Sword(30,20,"blade");
        arthur.setWeapon(weapon);
        arthur2 = new HpUp(arthur, 10);
        arthur3 = new ManaUp(arthur, 10);
        arthur4 = new ManaUp(arthur2, 10);
    }

    @Test
    public void testGetter() {
        assertEquals(3300, arthur2.getMaxHP());
        assertEquals(500, arthur2.getMaxMana());
        assertEquals("Apollonia", arthur2.getName());
        assertEquals("Saber", arthur2.getClassName());

        assertEquals(3000, arthur3.getMaxHP());
        assertEquals(550, arthur3.getMaxMana());
        assertEquals("Apollonia", arthur3.getName());
        assertEquals("Saber", arthur3.getClassName());

        assertEquals(3300, arthur4.getMaxHP());
        assertEquals(550, arthur4.getMaxMana());
        assertEquals("Apollonia", arthur4.getName());
        assertEquals("Saber", arthur4.getClassName());

        assertEquals(weapon,arthur2.getWeapon());
    }

    @Test
    public void testSetter() {
        arthur2.setHP(100);
        arthur2.setMana(40);
        assertEquals(100, arthur2.getHP());
        assertEquals(40, arthur2.getMana());
        arthur.setHP(-2);
        arthur.setMana(-3);
        assertEquals(0, arthur.getHP());
        assertEquals(0, arthur.getMana());
        arthur.setHP(10000);
        arthur.setMana(10000);
        assertEquals(3000, arthur.getHP());
        assertEquals(500, arthur.getMana());
        weapon=new Sword(15,10,"ultima");
        arthur2.setWeapon(weapon);
        assertEquals(weapon,arthur2.getWeapon());
    }


}

