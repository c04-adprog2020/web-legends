package com.c04.weblegends.core.character;

import com.c04.weblegends.core.character.*;
import com.c04.weblegends.core.weapon.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class RangerFactoryTest {
    CharFactory rangerFactory;
    @BeforeEach
    public void setUp(){
        rangerFactory=new RangerFactory();
    }
    @Test
    public void getterTest(){
        assertEquals("ranger",rangerFactory.getType());
    }
    @Test
    public void produceTest(){
        CharComponent chara=rangerFactory.produce("w");
        assertEquals("ranger",chara.getClassName());
        assertEquals("w",chara.getName());
        assertEquals("snipe",chara.getSpell().getName());
        assertNotNull(chara);
        assertTrue(chara.getWeapon() instanceof  Crossbow);
    }

}
