package com.c04.weblegends.core.character;

import com.c04.weblegends.core.character.*;
import com.c04.weblegends.core.weapon.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class MageFactoryTest {
    CharFactory mageFactory;
    @BeforeEach
    public void setUp(){
        mageFactory=new MageFactory();
    }
    @Test
    public void getterTest(){
        assertEquals("mage",mageFactory.getType());
    }
    @Test
    public void produceTest(){
        CharComponent chara=mageFactory.produce("w");
        assertEquals("mage",chara.getClassName());
        assertEquals("w",chara.getName());
        assertEquals("nova",chara.getSpell().getName());
        assertNotNull(chara);
        assertTrue(chara.getWeapon() instanceof  Staff);
    }

}
