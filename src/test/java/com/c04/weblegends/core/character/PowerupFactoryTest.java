package com.c04.weblegends.core.character;

import com.c04.weblegends.core.character.PowerupFactory;
import com.c04.weblegends.core.character.CharComponent;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PowerupFactoryTest {
    PowerupFactory powerupFactory;
    CharComponent mock;

    @BeforeEach
    public void setUp(){
        mock = new Saber("w");
        powerupFactory=new PowerupFactory();
    }

    @Test
    public void testFactory(){
        mock=powerupFactory.upgrade(mock);
        assertTrue(mock instanceof  Powerup);
        assertNotNull(mock);
    }
}
