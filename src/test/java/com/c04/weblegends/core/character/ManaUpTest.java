package com.c04.weblegends.core.character;

import com.c04.weblegends.core.character.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNull;

public class ManaUpTest {
    CharComponent arthur;

    @BeforeEach
    public void setUp() {
        arthur = new Saber("Apollonia");
        arthur = new ManaUp(arthur, 10);
    }


    @Test
    public void testGetter() {
        assertEquals(3000, arthur.getMaxHP());
        assertEquals(550, arthur.getMaxMana());
        assertEquals("Apollonia", arthur.getName());
        assertEquals("Saber", arthur.getClassName());
    }

    @Test
    public void testSetter() {
        arthur.setHP(100);
        arthur.setMana(40);
        assertEquals(100, arthur.getHP());
        assertEquals(40, arthur.getMana());
        arthur.setHP(-2);
        arthur.setMana(-3);
        assertEquals(0, arthur.getHP());
        assertEquals(0, arthur.getMana());
        arthur.setHP(10000);
        arthur.setMana(10000);
        assertEquals(3000, arthur.getHP());
        assertEquals(550, arthur.getMana());
    }


}

