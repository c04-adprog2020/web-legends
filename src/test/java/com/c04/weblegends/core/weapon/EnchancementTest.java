package com.c04.weblegends.core.weapon;

import com.c04.weblegends.core.weapon.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNull;

public class EnchancementTest {
    WeaponComponent weapon;

    @BeforeEach
    public void setUp() {
        weapon = new Sword(10000, 500, "Kusabimaru");
        weapon = new AttackUp(50, weapon);
        weapon = new DefenseUp(10, weapon);
    }

    @Test
    public void GetterTest() {
        assertEquals(15000, weapon.getAttack());
        assertEquals(550, weapon.getDefense());
        assertEquals("Kusabimaru", weapon.getName());
        assertEquals("sword", weapon.getType());
        assertEquals("sword", weapon.getType());
    }
}
