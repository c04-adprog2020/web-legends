package com.c04.weblegends.core.weapon;

import com.c04.weblegends.core.weapon.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNull;

public class DefenseUpTest {
    WeaponComponent weapon;

    @BeforeEach
    public void setUp() {
        weapon = new Sword(100000, 900, "Uchigatana");
        weapon = new DefenseUp(10, weapon);
    }

    @Test
    public void GetterTest() {
        assertEquals(100000, weapon.getAttack());
        assertEquals(990, weapon.getDefense());
        assertEquals("Uchigatana", weapon.getName());
        assertEquals("sword", weapon.getType());
    }
}
