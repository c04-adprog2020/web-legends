package com.c04.weblegends.core.weapon;

import com.c04.weblegends.core.weapon.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class SwordTest {
    WeaponComponent weapon;
    @BeforeEach
    public void setUp(){
        weapon=new Sword(30,20,"s");
    }
    @Test
    public void getterTest(){
        assertEquals("s",weapon.getName());
        assertEquals(30,weapon.getAttack());
        assertEquals(20,weapon.getDefense());
        assertEquals("sword",weapon.getType());
    }
}
