package com.c04.weblegends.core.character;

import com.c04.weblegends.core.weapon.Enhancement;
import com.c04.weblegends.core.weapon.EnhancementFactory;
import com.c04.weblegends.core.weapon.Sword;
import com.c04.weblegends.core.weapon.WeaponComponent;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class EnhancementFactoryTest {
    EnhancementFactory enhancementFactory;
    WeaponComponent mock;

    @BeforeEach
    public  void setUp(){
        mock = new Sword(20,10,"naginata");
        enhancementFactory=new EnhancementFactory();
    }

    @Test
    public void testFactory(){
        mock=enhancementFactory.upgrade(mock);
        assertTrue(mock instanceof  Enhancement);
        assertNotNull(mock);
    }
}
