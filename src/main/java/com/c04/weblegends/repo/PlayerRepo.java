package com.c04.weblegends.repo;

import com.c04.weblegends.core.character.*;
import com.c04.weblegends.core.weapon.*;
import java.util.*;

public class PlayerRepo {
    private Map<String, CharFactory> classFactories;

    /**
     * Constructor for PlayerRepo.
     */
    public PlayerRepo() {
        this.classFactories = new HashMap<String, CharFactory>();
        this.classFactories.put("archer", new ArcherFactory());
        this.classFactories.put("knight", new KnightFactory());
        this.classFactories.put("cleric", new ClericFactory());
        this.classFactories.put("mage", new MageFactory());
        this.classFactories.put("ranger", new RangerFactory());
        this.classFactories.put("assassin", new AssassinFactory());
    }

    public CharFactory getFactoryByName(String className) {
        return this.classFactories.get(className);
    }

    public List<CharFactory> getFactories() {
        return new ArrayList<CharFactory>(this.classFactories.values());
    }

    /**
     * Add new kind of factory to repository.
     *
     * @param name : Name of the factory
     * @param charFactory : Character Factory Object
     */
    public void addFactory(String name, CharFactory charFactory) {
        this.classFactories.put(name, charFactory);
    }

}
