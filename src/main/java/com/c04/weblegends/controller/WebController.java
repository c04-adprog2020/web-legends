package com.c04.weblegends.controller;

import com.c04.weblegends.service.PlayerService;
import com.c04.weblegends.service.PlayerServiceImpl;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping(path = "/")
public class WebController {
    /**
     * Landing page.
     *
     * @param model : Model object
     * @return landing.html
     */
    @GetMapping(path = "")
    public String landingPage(Model model) {
        return "character/landing";
    }

    /**
     * Redirect from home to character creation section.
     *
     * @param model              : Model object
     * @param redirectAttributes : playerService
     * @return /character/save url
     */
    @GetMapping(path = "tochar")
    public String toCharacter(Model model, RedirectAttributes redirectAttributes) {
        PlayerService playerService = new PlayerServiceImpl();
        redirectAttributes.addFlashAttribute("service", playerService);
        return "redirect:/character/save";
    }
}

