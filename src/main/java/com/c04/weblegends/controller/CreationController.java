package com.c04.weblegends.controller;

import com.c04.weblegends.service.BattleService;
import com.c04.weblegends.service.BattleServiceImpl;
import com.c04.weblegends.service.PlayerService;
import com.c04.weblegends.service.PlayerServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
@RequestMapping(path = "/character")
    public class CreationController {
        PlayerService playerService;

        /**
         * Routing for receiver on transmission process.
         *
         * @param playerService : Player Service Object
         * @param model         : Model
         * @return /character/ url
         */
        @GetMapping("/save")
        public String receive(@ModelAttribute("service") PlayerService playerService, Model model) {
            this.playerService = playerService;
            this.playerService.changeObject();
            return "redirect:/character/";
        }

        /**
         * Routing for main page in character.
         *
         * @param model : Model object
         * @return home.html
         */
        @GetMapping("/")
        public String homeCreation(Model model) {
            model.addAttribute("player", this.playerService.getHero());
            model.addAttribute("repo", this.playerService.getRepo());
            return "character/home";
        }

        /**
         * Routing to save received input.
         *
         * @param heroClass : Hero class
         * @param heroName  : Hero name
         * @param model     : Model object
         * @return /character/ url
         */
        @PostMapping("/produce")
        public String createClass(@RequestParam(value = "heroClass") String heroClass,
                                  @RequestParam(value = "heroName") String heroName,
                                  Model model
        ) {

            this.playerService.createHero(heroName, heroClass);
            return "redirect:/character/";
        }

        /**
         * Routing for sender on transmission process.
         *
         * @param model              : Model object
         * @param redirectAttributes : Object that's wanted to pass
         * @return /battle/save routing
         */
        @GetMapping("/battle/")
        public String toBattle(Model model, RedirectAttributes redirectAttributes) {
            BattleService battleService = new BattleServiceImpl(this.playerService);
            redirectAttributes.addFlashAttribute("service", battleService);
            return "redirect:/battle/save";
        }
    }
