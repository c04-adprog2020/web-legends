package com.c04.weblegends.controller;

import com.c04.weblegends.service.BattleService;
import com.c04.weblegends.service.EnemyService;
import com.c04.weblegends.service.PlayerService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
@RequestMapping(path = "/battle")
public class BattleController {
    private BattleService battleService;

    /**
     * Update model.
     *
     * @param model Model object
     * @return
     */
    private Model update(Model model) {
        PlayerService playerService = this.battleService.getPlayer();
        EnemyService enemyService = this.battleService.getEnemy();
        model.addAttribute("enemy", enemyService.getEnemy());
        model.addAttribute("player", playerService.getHero());
        model.addAttribute("weapon", playerService.getWeapon());
        model.addAttribute("items", playerService.getItems());
        return model;
    }

    /**
     * Routing for service transfer item => battle (receiver).
     *
     * @param battleService battleService object
     * @param model         model object
     * @return redirected url
     */
    @GetMapping(path = "/save")
    public String saveService(@ModelAttribute("service") BattleService battleService, Model model) {
        this.battleService = battleService;
        this.battleService.changeStage();
        return "redirect:/battle/";
    }

    /**
     * Routing for battle stage.
     *
     * @param model model object
     * @return battle.html template
     */
    @GetMapping(path = "/")
    public String battleScreen(Model model) {
        PlayerService playerService = this.battleService.getPlayer();
        EnemyService enemyService = this.battleService.getEnemy();
        model.addAttribute("enemy", enemyService.getEnemy());
        model.addAttribute("player", playerService.getHero());
        model.addAttribute("weapon", playerService.getWeapon());
        model.addAttribute("items", playerService.getItems());
        model.addAttribute("playerAttackLog", "");
        model.addAttribute("enemyAttackLog", "");
        return "character/battle";
    }

    /**
     * Routing to use item.
     *
     * @param itemIndex Index of itemList
     * @param model     model object
     * @return /battle/ url
     */
    @PostMapping(path = "/use")
    public String useItem(@RequestParam(value = "itemIndex") int itemIndex, Model model) {
        PlayerService playerService = this.battleService.getPlayer();
        playerService.useItem(itemIndex);
        return "redirect:/battle/";
    }

    /**
     * Routing for battle => item transmission (sender).
     *
     * @param model              Model object
     * @param redirectAttributes transferred object(playerService)
     * @return "/item/save" url
     */
    @GetMapping(path = "/item")
    public String redirectToItem(Model model, RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("service", this.battleService);
        return "redirect:/item/save";
    }

    /**
     * Method to use attack.
     *
     * @param model : Model object
     * @return Changed element
     */
    @GetMapping("/attack/")
    public String doAttack(Model model) {
        String playerAttackLog = this.battleService.playerAttack();
        String enemyAttackLog = this.battleService.enemyAttack();
        model = update(model);
        model.addAttribute("playerAttackLog", playerAttackLog);
        model.addAttribute("enemyAttackLog", enemyAttackLog);
        return "character/battle ::  #gameStat";
    }

    @GetMapping("/skill/")
    public String doSkill(Model model){
        String playerAttackLog = this.battleService.playerSpell();
        String enemyAttackLog = this.battleService.enemyAttack();
        model = update(model);
        model.addAttribute("playerAttackLog", playerAttackLog);
        model.addAttribute("enemyAttackLog", enemyAttackLog);
        return "character/battle ::  #gameStat";
    }
}
