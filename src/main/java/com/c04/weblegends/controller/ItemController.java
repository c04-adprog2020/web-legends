package com.c04.weblegends.controller;

import com.c04.weblegends.core.item.Item;
import com.c04.weblegends.service.BattleService;
import com.c04.weblegends.service.PlayerService;
import com.c04.weblegends.service.PlayerServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping(path = "/item")
public class ItemController {

    private BattleService battleService;

    /**
     * Routing for battle => item transmission (receiver).
     *
     * @param battleService received object
     * @param model         object model
     * @return /battle/save url
     */
    @GetMapping(path = "/save")
    public String saveService(@ModelAttribute("service") BattleService battleService,
                              Model model, RedirectAttributes redirectAttributes) {
        this.battleService = battleService;
        this.battleService.changeObject();
        PlayerService playerService = battleService.getPlayer();
        Item item = playerService.getItem();
        String upgradeLog = playerService.doPowerUp();
        String enhanceLog = playerService.doEnhanceWeapon();
        redirectAttributes.addFlashAttribute("service", this.battleService);
        return "redirect:/battle/save";
    }


}
