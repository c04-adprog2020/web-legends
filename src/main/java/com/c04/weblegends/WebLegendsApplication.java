package com.c04.weblegends;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebLegendsApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebLegendsApplication.class, args);
    }

}
