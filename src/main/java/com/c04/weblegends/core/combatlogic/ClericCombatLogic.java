package com.c04.weblegends.core.combatlogic;

import com.c04.weblegends.core.aicharacter.*;

public class ClericCombatLogic extends CombatLogic {
    public ClericCombatLogic(ArtificialIntelligence character) {
        super(character);
    }

    @Override
    public String useSkill() {
        return (this.character.getSkillCooldown() == 0) ? "Heal" : "Attack";
    }

    @Override
    public String move() {
        if (this.character.getHP() > 0.9 * this.character.getMaxHP()) {
            return "Attack";
        } else {
            return useSkill();
        }
    }
}