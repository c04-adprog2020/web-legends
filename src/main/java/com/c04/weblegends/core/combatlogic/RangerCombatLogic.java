package com.c04.weblegends.core.combatlogic;

import com.c04.weblegends.core.aicharacter.*;

public class RangerCombatLogic extends CombatLogic {
    public RangerCombatLogic(ArtificialIntelligence character) {
        super(character);
    }

    @Override
    public String useSkill() {
        return (this.character.getSkillCooldown() == 0) ? "Multishot" : "Attack";
    }

    @Override
    public String move() {
        return useSkill();
    }
}