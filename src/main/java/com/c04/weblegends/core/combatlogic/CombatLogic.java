package com.c04.weblegends.core.combatlogic;

import com.c04.weblegends.core.aicharacter.*;

public abstract class CombatLogic {
    public ArtificialIntelligence character;

    public CombatLogic(ArtificialIntelligence character) {
        this.character = character;
    }

    public abstract String useSkill();

    public abstract String move();
}