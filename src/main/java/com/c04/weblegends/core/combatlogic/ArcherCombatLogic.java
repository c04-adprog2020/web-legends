package com.c04.weblegends.core.combatlogic;

import com.c04.weblegends.core.aicharacter.*;

public class ArcherCombatLogic extends CombatLogic {
    public ArcherCombatLogic(ArtificialIntelligence character) {
        super(character);
    }

    @Override
    public String useSkill() {
        return (this.character.getSkillCooldown() == 0) ? "True Strike" : "Attack";
    }

    @Override
    public String move() {
        return useSkill();
    }
}