package com.c04.weblegends.core.combatlogic;

import com.c04.weblegends.core.aicharacter.*;

public class MageCombatLogic extends CombatLogic {
    public MageCombatLogic(ArtificialIntelligence character) {
        super(character);
    }

    @Override
    public String useSkill() {
        return (this.character.getSkillCooldown() == 0) ? "Life Drain" : "Attack";
    }

    @Override
    public String move() {
        if (this.character.getHP() > 0.9 * this.character.getMaxHP()) {
            return "Attack";
        } else {
            return useSkill();
        }
    }
}