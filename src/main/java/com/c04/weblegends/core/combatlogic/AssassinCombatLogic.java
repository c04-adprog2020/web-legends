package com.c04.weblegends.core.combatlogic;

import com.c04.weblegends.core.aicharacter.*;

public class AssassinCombatLogic extends CombatLogic {
    public AssassinCombatLogic(ArtificialIntelligence character) {
        super(character);
    }

    @Override
    public String useSkill() {
        return (this.character.getSkillCooldown() == 0) ? "Critical Strike" : "Attack";
    }

    @Override
    public String move() {
        return useSkill();
    }
}