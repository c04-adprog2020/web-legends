package com.c04.weblegends.core.combatlogic;

import com.c04.weblegends.core.aicharacter.*;

public class KnightCombatLogic extends CombatLogic {
    public KnightCombatLogic(ArtificialIntelligence character) {
        super(character);
    }

    @Override
    public String useSkill() {
        return (this.character.getSkillCooldown() == 0) ? "Pierce Attack" : "Attack";
    }

    @Override
    public String move() {
        return useSkill();
    }
}