package com.c04.weblegends.core.aicharacter;

import com.c04.weblegends.core.character.Character;
import com.c04.weblegends.core.combatlogic.*;
import java.util.Random;

public class AssassinAI extends ArtificialIntelligence {
    /** 
    * Class constructor.
    */
    public AssassinAI(int level) {
        this.maxHP = 1800 + 105 * level;
        this.currentHP = this.maxHP;
        this.skillCooldown = 0;
        this.level = level;
        this.atkPoint = 27 + 9 * level;
        this.defPoint = 5 + 3 * level;
        this.critRate = 13 + 3 * level;
        this.accuracy = 75 + 3 * level;
        this.combatLogic = new AssassinCombatLogic(this);
        this.characterClass = "Assassin";
    }

    @Override
    public void executeMove() {
        this.currentMove = this.combatLogic.move();
        if (this.currentMove.equals("Attack")) {
            this.moveDescription = "Attack with my Dagger";
        } else {
            this.moveDescription = "Feel the deep wound";
        }
    }
}