package com.c04.weblegends.core.aicharacter;

import com.c04.weblegends.core.character.Character;
import com.c04.weblegends.core.combatlogic.*;
import java.util.Random;

public class ClericAI extends ArtificialIntelligence {
    /** 
    * Class constructor.
    */
    public ClericAI(int level) {
        this.maxHP = 2100 + 145 * level;
        this.currentHP = this.maxHP;
        this.skillCooldown = 0;
        this.level = level;
        this.atkPoint = 22 + 6 * level;
        this.defPoint = 10 + 4 * level;
        this.critRate = 8 + 2 * level;
        this.accuracy = 68 + 3 * level;
        this.combatLogic = new ClericCombatLogic(this);
        this.characterClass = "Cleric";
    }

    @Override
    public void executeMove() {
        this.currentMove = this.combatLogic.move();
        if (this.currentMove.equals("Attack")) {
            this.moveDescription = "Attack with my Mace";
        } else {
            this.moveDescription = "Healing for survival";
        }
    }
}