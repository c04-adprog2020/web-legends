package com.c04.weblegends.core.aicharacter;

import com.c04.weblegends.core.character.Character;
import com.c04.weblegends.core.combatlogic.*;
import java.util.Random;

public class RangerAI extends ArtificialIntelligence {
    /** 
    * Class constructor.
    */
    public RangerAI(int level) {
        this.maxHP = 2000 + 125 * level;
        this.currentHP = this.maxHP;
        this.skillCooldown = 0;
        this.level = level;
        this.atkPoint = 26 + 7 * level;
        this.defPoint = 7 + 1 * level;
        this.critRate = 10 + 3 * level;
        this.accuracy = 72 + 3 * level;
        this.combatLogic = new RangerCombatLogic(this);
        this.characterClass = "Ranger";
    }

    @Override
    public void executeMove() {
        this.currentMove = this.combatLogic.move();
        if (this.currentMove.equals("Attack")) {
            this.moveDescription = "Attack with my Shotgun";
        } else {
            this.moveDescription = "Triple barrel at a time";
        }
    }
}