package com.c04.weblegends.core.aicharacter;

import com.c04.weblegends.core.character.Character;
import com.c04.weblegends.core.combatlogic.*;
import java.util.Random;

public class MageAI extends ArtificialIntelligence {
    /** 
    * Class constructor.
    */
    public MageAI(int level) {
        this.maxHP = 1800 + 120 * level;
        this.currentHP = this.maxHP;
        this.skillCooldown = 0;
        this.level = level;
        this.atkPoint = 35 + 7 * level;
        this.defPoint = 11 + 1 * level;
        this.critRate = 14 + 2 * level;
        this.accuracy = 77 + 2 * level;
        this.combatLogic = new MageCombatLogic(this);
        this.characterClass = "Mage";
    }

    @Override
    public void executeMove() {
        this.currentMove = this.combatLogic.move();
        if (this.currentMove.equals("Attack")) {
            this.moveDescription = "Attack with my Staff";
        } else {
            this.moveDescription = "I will drain your life";
        }
    }
}