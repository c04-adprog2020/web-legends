package com.c04.weblegends.core.aicharacter;

import com.c04.weblegends.core.character.Character;
import com.c04.weblegends.core.combatlogic.*;
import java.util.Random;

public class ArcherAI extends ArtificialIntelligence {
    /** 
    * Class constructor.
    */
    public ArcherAI(int level) {
        this.maxHP = 2000 + 120 * level;
        this.currentHP = this.maxHP;
        this.skillCooldown = 0;
        this.level = level;
        this.atkPoint = 25 + 7 * level;
        this.defPoint = 7 + 2 * level;
        this.critRate = 11 + 3 * level;
        this.accuracy = 70 + 3 * level;
        this.combatLogic = new ArcherCombatLogic(this);
        this.characterClass = "Archer";
    }

    @Override
    public void executeMove() {
        this.currentMove = this.combatLogic.move();
        if (this.currentMove.equals("Attack")) {
            this.moveDescription = "Attack with my Bow";
        } else {
            this.moveDescription = "My attack never miss";
        }
    }
}