package com.c04.weblegends.core.aicharacter;

import com.c04.weblegends.core.character.Character;
import com.c04.weblegends.core.character.Knight;
import com.c04.weblegends.core.combatlogic.*;
import java.util.Random;

public class KnightAI extends ArtificialIntelligence {
    /** 
    * Class constructor.
    */
    public KnightAI(int level) {
        this.maxHP = 2050 + 140 * level;
        this.currentHP = this.maxHP;
        this.skillCooldown = 0;
        this.level = level;
        this.atkPoint = 25 + 6 * level;
        this.defPoint = 10 + 4 * level;
        this.critRate = 10 + 2 * level;
        this.accuracy = 70 + 2 * level;
        this.combatLogic = new KnightCombatLogic(this);
        this.characterClass = "Knight";
    }

    @Override
    public void executeMove() {
        this.currentMove = this.combatLogic.move();
        if (this.currentMove.equals("Attack")) {
            this.moveDescription = "Attack with my Sword";
        } else {
            this.moveDescription = "This sword pierce throught anything";
        }
    }
}