package com.c04.weblegends.core.weapon;

public abstract class Weapon implements WeaponComponent {
    protected int defPoint;
    protected int atkPoint;
    protected String name;

    /**
     * Constructor Weapon.
     *
     * @param atkPoint Damage value of weapon
     * @param defPoint Defense value of weapon
     * @param name     Name of the weapon
     */
    public Weapon(int atkPoint, int defPoint, String name) {
        this.defPoint = defPoint;
        this.atkPoint = atkPoint;
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getAttack() {
        return this.atkPoint;
    }

    @Override
    public int getDefense() {
        return this.defPoint;
    }

    public abstract String getType();

}