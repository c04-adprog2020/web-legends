package com.c04.weblegends.core.weapon;


public class AttackUp extends Enhancement {
    private int atkBuff;

    /**
     * Method to generate additional damage.
     *
     * @param percent Enhancement percentage
     * @param weapon  Weapon object
     * @return enhancement value
     */
    private static int getAttackPoint(int percent, WeaponComponent weapon) {
        return (int) Math.round(((float) weapon.getAttack() * (float) percent) / (float) 100);
    }

    /**
     * Constructor AttackUp.
     *
     * @param percentage Enhancement percentage
     * @param weapon     Weapon that's wanted to be buffed
     */
    public AttackUp(int percentage, WeaponComponent weapon) {
        super(weapon);
        this.atkBuff = getAttackPoint(percentage, weapon);
    }

    public int getAttack() {
        return this.atkBuff + this.weapon.getAttack();
    }


    public int getDefense() {
        return this.weapon.getDefense();
    }


}
