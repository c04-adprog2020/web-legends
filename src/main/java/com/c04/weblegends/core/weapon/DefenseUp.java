package com.c04.weblegends.core.weapon;


public class DefenseUp extends Enhancement {
    private int defBuff;

    /**
     * Method to generate additional defense.
     *
     * @param percent Enhancement percentage
     * @param weapon  Weapon object
     * @return enhancement value
     */
    private static int getDefensePoint(int percent, WeaponComponent weapon) {
        return (int) Math.round(((float) weapon.getDefense() * (float) percent) / (float) 100);
    }

    /**
     * Constructor DefenseUp.
     *
     * @param percentage Enhancement percentage
     * @param weapon     Weapon that's wanted to be buffed
     */
    public DefenseUp(int percentage, WeaponComponent weapon) {
        super(weapon);
        this.defBuff = getDefensePoint(percentage, weapon);
    }

    public int getAttack() {
        return this.weapon.getAttack();
    }


    public int getDefense() {
        return this.defBuff + this.weapon.getDefense();
    }


    public String getName() {
        return this.weapon.getName();
    }

    public String getType() {
        return this.weapon.getType();
    }
}
