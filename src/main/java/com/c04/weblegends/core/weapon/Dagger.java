package com.c04.weblegends.core.weapon;

public class Dagger extends Weapon {
    /**
     * Constructor for dagger weapon.
     *
     * @param atk  : Damage point
     * @param def  : Defense point
     * @param name : Name of the weapon
     */
    public Dagger(int atk, int def, String name) {
        super(atk, def, name);
    }

    @Override
    public String getType() {
        return "dagger";
    }
}
