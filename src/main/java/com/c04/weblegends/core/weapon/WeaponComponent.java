package com.c04.weblegends.core.weapon;

public interface WeaponComponent {
    public abstract int getAttack();

    public abstract String getName();

    public abstract String getType();

    public abstract int getDefense();

    public abstract String toString();
}
