package com.c04.weblegends.core.weapon;

public class Bow extends Weapon {
    /**
     * Constructor for bow weapon.
     *
     * @param atk  : Damage point
     * @param def  : Defense point
     * @param name : Name of the weapon
     */
    public Bow(int atk, int def, String name) {
        super(atk, def, name);
    }

    @Override
    public String getType() {
        return "bow";
    }
}
