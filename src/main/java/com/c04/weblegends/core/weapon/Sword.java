package com.c04.weblegends.core.weapon;

public class Sword extends Weapon {
    /**
     * Constructor for sword weapon.
     *
     * @param atk  : Damage point
     * @param def  : Defense point
     * @param name : Name of the weapon
     */
    public Sword(int atk, int def, String name) {
        super(atk, def, name);
    }

    @Override
    public String getType() {
        return "sword";
    }
}
