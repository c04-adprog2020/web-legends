package com.c04.weblegends.core.weapon;

import com.c04.weblegends.core.character.CharComponent;
import com.c04.weblegends.core.character.HpUp;
import com.c04.weblegends.core.character.ManaUp;
import java.util.Random;

public class EnhancementFactory {
    /**
     * Method to do powerup on weapon(randomly).
     *
     * @param weapon weapon object
     * @return upgraded weapon
     */
    public WeaponComponent upgrade(WeaponComponent weapon) {
        int rng = new Random().nextInt(2);
        int percent = new Random().nextInt(100) + 1;
        System.out.println(rng);
        WeaponComponent upgraded;
        if (rng == 0) {
            upgraded = new AttackUp(percent, weapon);
        } else if (rng == 1) {
            upgraded = new DefenseUp(percent, weapon);
        } else {
            upgraded = weapon;
        }
        return upgraded;
    }
}
