package com.c04.weblegends.core.weapon;

public abstract class Enhancement implements WeaponComponent {
    WeaponComponent weapon;

    /**
     * Constructor Enchancement.
     *
     * @param weapon Weapon object that's wanted to buffed
     */
    public Enhancement(WeaponComponent weapon) {
        this.weapon = weapon;
    }

    public String getName() {
        return this.weapon.getName();
    }

    public String getType() {
        return this.weapon.getType();
    }

}
