package com.c04.weblegends.core.weapon;

public class Staff extends Weapon {
    /**
     * Constructor for staff weapon.
     *
     * @param atk  : Damage point
     * @param def  : Defense point
     * @param name : Name of the weapon
     */
    public Staff(int atk, int def, String name) {
        super(atk, def, name);
    }

    @Override
    public String getType() {
        return "staff";
    }
}
