package com.c04.weblegends.core.weapon;

public class Crossbow extends Weapon {
    /**
     * Constructor for crossbow weapon.
     *
     * @param atk  : Damage point
     * @param def  : Defense point
     * @param name : Name of the weapon
     */
    public Crossbow(int atk, int def, String name) {
        super(atk, def, name);
    }

    @Override
    public String getType() {
        return "crossbow";
    }
}
