package com.c04.weblegends.core.item;

import com.c04.weblegends.core.character.CharComponent;

public class Elixir implements Item {
    CharComponent character;
    int addMana;

    /**
     * Contructor Elixir.
     *
     * @param character Player object
     * @param addMana   Additional MP
     */
    public Elixir(CharComponent character, int addMana) {
        this.character = character;
        this.addMana = addMana;
    }

    public String getType() {
        return "elixir";
    }

    public int getMana() {
        return this.addMana;
    }

    public int getHP() {
        return 0;
    }

    /**
     * Consume the item.
     */
    public void use() {
        this.character.setMana(this.character.getMana() + this.addMana);
    }

    /**
     * Change player object method.
     *
     * @param character Player object 
     */
    public void setPlayer(CharComponent character) {
        this.character = character;
    }

    public CharComponent getPlayer() {
        return this.character;
    }
}
