package com.c04.weblegends.core.item;


import com.c04.weblegends.core.character.CharComponent;

public interface Item {
    public abstract void use();

    public abstract String getType();

    public abstract int getMana();

    public abstract int getHP();

    public abstract void setPlayer(CharComponent character);

    public abstract CharComponent getPlayer();
}
