package com.c04.weblegends.core.item;

import com.c04.weblegends.core.character.CharComponent;
import java.util.Random;

public class ItemFactory {
    CharComponent character;
    private static String[] itemRepo = new String[]{"potion", "elixir"};

    /**
     * Random drop RNG.
     *
     * @param upperBound Number of item types
     * @return index value
     */
    private static int getRandomValue(int upperBound) {
        return new Random().nextInt(upperBound);
    }

    /**
     * Constructor itemFactory.
     *
     * @param character Player object
     */
    public ItemFactory(CharComponent character) {
        this.character = character;
    }

    /**
     * Method to create item (random drop).
     *
     * @return Item
     */
    public Item createItem() {
        Item item;
        int rng = new Random().nextInt(2);
        if (rng == 0) {
            int hpVal = new Random().nextInt(this.character.getMaxHP() - 1) + 1;
            item = new Potion(this.character, hpVal);
        } else if (rng == 1) {
            int manaVal = new Random().nextInt(this.character.getMaxMana() - 1) + 1;
            item = new Elixir(this.character, manaVal);
        } else {
            item = null;
        }
        return item;
    }

    /**
     * Method to adjust  new object change for controller.
     *
     * @param character Player object
     */
    public void setCharacter(CharComponent character) {
        this.character = character;
    }

    public CharComponent getPlayer() {
        return this.character;
    }

}