package com.c04.weblegends.core.item;

import com.c04.weblegends.core.character.CharComponent;

public class Potion implements Item {
    CharComponent character;
    int addHP;

    /**
     * Constructor Potion.
     *
     * @param character Player object
     * @param addHP     Additional HP
     */
    public Potion(CharComponent character, int addHP) {
        this.character = character;
        this.addHP = addHP;
    }

    public String getType() {
        return "potion";
    }

    public int getMana() {
        return 0;
    }

    public int getHP() {
        return this.addHP;
    }

    /**
     * Consume the item.
     */
    public void use() {
        this.character.setHP(this.character.getHP() + this.addHP);
    }

    public void setPlayer(CharComponent character) {
        this.character = character;
    }

    public CharComponent getPlayer() {
        return this.character;
    }


}
