package com.c04.weblegends.core.character;

import java.util.Random;

public class PowerupFactory {
    /**
     * Method to do powerup on player(randomly).
     *
     * @param character player object
     * @return upgraded player
     */
    public CharComponent upgrade(CharComponent character) {
        int rng = new Random().nextInt(2);
        int percent = new Random().nextInt(100) + 1;
        CharComponent upgraded;
        System.out.println(rng);
        if (rng == 0) {
            upgraded = new HpUp(character, percent);
        } else if (rng == 1) {
            upgraded = new ManaUp(character, percent);
        } else {
            upgraded = character;
        }
        return upgraded;
    }

}
