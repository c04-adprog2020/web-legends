package com.c04.weblegends.core.character;

import com.c04.weblegends.core.weapon.*;

public class Mage extends Character {
    /**
     * Constructor for mage class.
     *
     * @param name : Name of player
     */
    public Mage(String name) {
        super(name, 1000, 2500,new Spell("nova",500,3));
    }

    @Override
    public String getClassName() {
        return "mage";
    }

    /**
     * Change weapon method.
     *
     * @param weapon Weapon
     */
    @Override
    public void setWeapon(WeaponComponent weapon) {
        this.weapon = weapon;
    }

}