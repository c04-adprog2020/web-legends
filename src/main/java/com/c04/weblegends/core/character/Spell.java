package com.c04.weblegends.core.character;

public class Spell {
    protected String name;
    protected int mana;
    protected int cooldown;

    public Spell(String name, int mana, int cooldown){
        this.name = name;
        this.mana = mana;
        this.cooldown = cooldown;
    }

    public String getName(){
        return this.name;
    }

    public int getMana(){
        return this.mana;
    }

    public int getCooldown(){
        return this.cooldown;
    }
}
