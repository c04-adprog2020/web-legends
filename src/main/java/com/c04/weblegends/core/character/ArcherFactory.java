package com.c04.weblegends.core.character;

import com.c04.weblegends.core.weapon.*;

public class ArcherFactory implements CharFactory {
    /**
     * Produce archer class.
     *
     * @param name : Name of the hero
     * @return character
     */
    public CharComponent produce(String name) {
        CharComponent chara = new Archer(name);
        chara.setWeapon(new Bow(100, 10, "Archer Bow"));
        return chara;
    }

    @Override
    public String getType() {
        return "archer";
    }
}
