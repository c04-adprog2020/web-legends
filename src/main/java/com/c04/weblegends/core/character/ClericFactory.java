package com.c04.weblegends.core.character;

import com.c04.weblegends.core.weapon.*;

public class ClericFactory implements CharFactory {
    /**
     * Produce cleric class.
     *
     * @param name : Name of the hero
     * @return character
     */
    public CharComponent produce(String name) {
        CharComponent chara = new Cleric(name);
        chara.setWeapon(new Staff(12, 0, "Cleric Staff"));
        return chara;
    }

    public String getType() {
        return "cleric";
    }
}
