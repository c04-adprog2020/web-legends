package com.c04.weblegends.core.character;

import com.c04.weblegends.core.weapon.*;

public class Assassin extends Character {
    /**
     * Constructor for assassin class.
     *
     * @param name : Name of player
     */
    public Assassin(String name) {
        super(name, 500, 200, new Spell("backstab",100,3));
    }

    @Override
    public String getClassName() {
        return "assassin";
    }

    /**
     * Change weapon method.
     *
     * @param weapon Weapon
     */
    @Override
    public void setWeapon(WeaponComponent weapon) {
        this.weapon = weapon;
    }

}