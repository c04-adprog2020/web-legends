package com.c04.weblegends.core.character;

import com.c04.weblegends.core.weapon.*;

public class Cleric extends Character {
    /**
     * Constructor for Cleric class.
     *
     * @param name : Name of player
     */
    public Cleric(String name) {
        super(name, 1500, 2000,new Spell("heal",400,3));
    }

    @Override
    public String getClassName() {
        return "cleric";
    }

    /**
     * Change weapon method.
     *
     * @param weapon Weapon
     */
    @Override
    public void setWeapon(WeaponComponent weapon) {
        this.weapon = weapon;
    }

}