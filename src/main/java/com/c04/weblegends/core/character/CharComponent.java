package com.c04.weblegends.core.character;

import com.c04.weblegends.core.item.*;
import com.c04.weblegends.core.weapon.*;
import java.lang.reflect.Array;
import java.util.*;

public abstract class CharComponent {
    protected int hp;
    protected int mana;
    protected ItemFactory itemFactory;
    protected ArrayList<Item> itemList;
    protected Spell spell;
    protected int spellCooldown;

    /**
     * constructor for CharComponent object.
     *
     * @param hp   Initial HP
     * @param mana Initial MP
     */
    public CharComponent(int hp, int mana) {
        itemList = new ArrayList<Item>();
        this.hp = hp;
        this.mana = mana;
        this.itemFactory = new ItemFactory(this);
        this.spellCooldown = 0;
    }

    /**
     * Method to update player HP.
     *
     * @param hp HP that's wanted to be set
     */
    public void setHP(int hp) {
        if (hp > 0 && hp <= getMaxHP()) {
            this.hp = hp;
        } else if (hp <= 0) {
            this.hp = 0;
        } else {
            this.hp = getMaxHP();
        }
    }

    /**
     * Method to update player MP.
     *
     * @param mana MP that's wanted to be set
     */
    public void setMana(int mana) {
        if (mana > 0 && mana <= getMaxMana()) {
            this.mana = mana;
        } else if (mana <= 0) {
            this.mana = 0;
        } else {
            this.mana = getMaxMana();
        }
    }

    public int getHP() {
        return this.hp;
    }

    public int getMana() {
        return this.mana;
    }

    public Spell getSpell(){ return this.spell; }

    public int getSpellCooldown(){ return this.spellCooldown; }

    public void setSpellCooldown(int cd){ this.spellCooldown = cd; }

    public abstract int getMaxHP();

    public abstract int getMaxMana();

    public abstract String getName();

    public abstract String getClassName();

    public abstract void setWeapon(WeaponComponent weapon);

    public abstract WeaponComponent getWeapon();

}
