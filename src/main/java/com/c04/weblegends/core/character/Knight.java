package com.c04.weblegends.core.character;

import com.c04.weblegends.core.weapon.*;

public class Knight extends Character {
    /**
     * Constructor for knight class.
     *
     * @param name : Name of player
     */
    public Knight(String name) {
        super(name, 2000, 500,new Spell("true strike",200,3));
    }

    @Override
    public String getClassName() {
        return "knight";
    }

    /**
     * Change weapon method.
     *
     * @param weapon Weapon
     */
    @Override
    public void setWeapon(WeaponComponent weapon) {
        this.weapon = weapon;
    }

}