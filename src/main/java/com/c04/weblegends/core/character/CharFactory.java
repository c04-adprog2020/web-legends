package com.c04.weblegends.core.character;

public interface CharFactory {
    public CharComponent produce(String name);

    public String getType();
}
