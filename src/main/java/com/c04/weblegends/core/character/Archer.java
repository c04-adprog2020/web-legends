package com.c04.weblegends.core.character;

import com.c04.weblegends.core.weapon.*;

public class Archer extends Character {
    /**
     * Constructor for archer class.
     *
     * @param name : Name of player
     */
    public Archer(String name) {
        super(name, 1800, 1000, new Spell("multishot",200,3));
    }

    @Override
    public String getClassName() {
        return "archer";
    }

    /**
     * Change weapon method.
     *
     * @param weapon Weapon
     */
    @Override
    public void setWeapon(WeaponComponent weapon) {
        this.weapon = weapon;
    }

}