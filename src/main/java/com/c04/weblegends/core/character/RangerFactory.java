package com.c04.weblegends.core.character;

import com.c04.weblegends.core.weapon.*;

public class RangerFactory implements CharFactory {
    /**
     * Produce ranger class.
     *
     * @param name : Name of the hero
     * @return character
     */
    public CharComponent produce(String name) {
        CharComponent chara = new Ranger(name);
        chara.setWeapon(new Crossbow(80, 8, "Ranger Crossbow"));
        return chara;
    }

    @Override
    public String getType() {
        return "ranger";
    }
}
