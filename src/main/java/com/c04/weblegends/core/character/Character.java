package com.c04.weblegends.core.character;

import com.c04.weblegends.core.weapon.*;

public abstract class Character extends CharComponent {
    protected int baseMaxHP;
    protected int baseMaxMana;
    protected String name;
    protected WeaponComponent weapon;
    protected Spell spell;

    /**
     * Constructor character.
     *
     * @param name        name of the player
     * @param baseMaxHP   player base maximum HP
     * @param baseMaxMana player base maximum MP
     */
    public Character(String name, int baseMaxHP, int baseMaxMana, Spell spell) {
        super(baseMaxHP, baseMaxMana);
        this.name = name;
        this.baseMaxHP = baseMaxHP;
        this.baseMaxMana = baseMaxMana;
        this.spell = spell;
    }

    @Override
    public int getMaxHP() {
        return this.baseMaxHP;
    }

    @Override
    public int getMaxMana() {
        return this.baseMaxMana;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public WeaponComponent getWeapon() {
        return this.weapon;
    }

    @Override
    public Spell getSpell(){ return this.spell; }



}