package com.c04.weblegends.core.character;

import com.c04.weblegends.core.weapon.*;

public class Saber extends Character {
    /**
     * Dummy class.
     *
     * @param name : Hero name
     */
    public Saber(String name) {
        super(name, 3000, 500, new Spell("slash",100,3));
    }

    @Override
    public String getClassName() {
        return "Saber";
    }

    /**
     * Change weapon method.
     *
     * @param weapon Weapon
     */
    @Override
    public void setWeapon(WeaponComponent weapon) {
        this.weapon = weapon;
    }

}