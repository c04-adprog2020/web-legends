package com.c04.weblegends.core.character;

import com.c04.weblegends.core.weapon.*;

public class Ranger extends Character {
    /**
     * Constructor for ranger class.
     *
     * @param name : Name of player
     */
    public Ranger(String name) {
        super(name, 1000, 500,new Spell("snipe",250,3));
    }

    @Override
    public String getClassName() {
        return "ranger";
    }

    /**
     * Change weapon method.
     *
     * @param weapon Weapon
     */
    @Override
    public void setWeapon(WeaponComponent weapon) {
        this.weapon = weapon;
    }

}