package com.c04.weblegends.core.character;

import com.c04.weblegends.core.weapon.*;

public abstract class Powerup extends CharComponent {
    protected CharComponent character;
    protected int addMaxHP;
    protected int addMaxMana;

    /**
     * Contructor Powerup.
     *
     * @param character  Player object
     * @param addMaxHP   Max HP Addition
     * @param addMaxMana Max MP Addidition
     */
    public Powerup(CharComponent character, int addMaxHP, int addMaxMana) {
        super(character.getHP(), character.getMana());
        this.character = character;
        this.addMaxHP = addMaxHP;
        this.addMaxMana = addMaxMana;
    }

    /**
     * Method to change character weapon.
     *
     * @param weapon : New weapon
     */
    @Override
    public void setWeapon(WeaponComponent weapon) {
        this.character.setWeapon(weapon);
    }

    @Override
    public String getName() {
        return this.character.getName();
    }

    @Override
    public String getClassName() {
        return this.character.getClassName();
    }

    @Override
    public WeaponComponent getWeapon() {
        return this.character.getWeapon();
    }

}
