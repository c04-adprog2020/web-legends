package com.c04.weblegends.core.character;

import com.c04.weblegends.core.weapon.*;

public class AssassinFactory implements CharFactory {
    /**
     * Produce assassin class.
     *
     * @param name : Name of the hero
     * @return character
     */
    public CharComponent produce(String name) {
        CharComponent chara = new Assassin(name);
        chara.setWeapon(new Dagger(75, 25, "Assassin Dagger"));
        return chara;
    }

    @Override
    public String getType() {
        return "assassin";
    }
}
