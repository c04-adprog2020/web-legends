package com.c04.weblegends.core.character;

public interface EnemyObj {
    public int getDamage();

    public int getHP();

    public int getMaxHP();

    public void setHP(int hp);

    public int getLevel();
}
