package com.c04.weblegends.core.character;


public class HpUp extends Powerup {
    /**
     * Method to generate additional HP.
     *
     * @param character Player object
     * @param percent   Percentage for additional (taken from current player Max HP)
     * @return Additional HP Value
     */
    private static int generateHP(CharComponent character, int percent) {
        return (int) Math.round(((float) character.getMaxHP() / (float) 100)) * percent;
    }

    /**
     * Contructor HPUp.
     *
     * @param character Player object
     * @param percent   Enchancement percentage
     */
    public HpUp(CharComponent character, int percent) {
        super(character, generateHP(character, percent), 0);
    }

    @Override
    public int getMaxHP() {
        return this.addMaxHP + this.character.getMaxHP();
    }

    @Override
    public int getMaxMana() {
        return this.character.getMaxMana();
    }

}