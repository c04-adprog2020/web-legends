package com.c04.weblegends.core.character;


public class ManaUp extends Powerup {
    /**
     * Method to generate additional MP.
     *
     * @param character Player object
     * @param percent   Percentage for additional (taken from current player Max MP)
     * @return Additional MP Value
     */
    private static int generateMana(CharComponent character, int percent) {
        return (int) Math.round(((float) character.getMaxMana() / (float) 100)) * percent;
    }

    /**
     * Constructor for ManaUp.
     *
     * @param character Player object
     * @param percent   Enchancement percentage
     */
    public ManaUp(CharComponent character, int percent) {
        super(character, 0, generateMana(character, percent));
    }

    @Override
    public int getMaxHP() {
        return this.character.getMaxHP();
    }

    @Override
    public int getMaxMana() {
        return this.addMaxMana + this.character.getMaxMana();
    }
}
