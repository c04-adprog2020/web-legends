package com.c04.weblegends.core.character;

import com.c04.weblegends.core.weapon.*;

public class MageFactory implements CharFactory {
    /**
     * Produce mage class.
     *
     * @param name : Name of the hero
     * @return character
     */
    public CharComponent produce(String name) {
        CharComponent chara = new Mage(name);
        chara.setWeapon(new Staff(25, 0, "Mage Staff"));
        return chara;
    }

    @Override
    public String getType() {
        return "mage";
    }
}
