package com.c04.weblegends.service;

import com.c04.weblegends.core.aicharacter.*;
import java.util.Random;

public class EnemyServiceImpl implements EnemyService {
    private ArtificialIntelligence enemy;

    /**
     * Spawn new enemy instance on certain stage.
     *
     * @param level : Level of the enemy
     */
    @Override
    public void spawnEnemy(int level) {
        Random r = new Random();
        int randomValue = r.nextInt(6);
        switch (randomValue) {
            case 0:
                this.enemy = new ArcherAI(level);
                break;
            case 1:
                this.enemy = new AssassinAI(level);
                break;
            case 2:
                this.enemy = new ClericAI(level);
                break;
            case 3:
                this.enemy = new KnightAI(level);
                break;
            case 4:
                this.enemy = new MageAI(level);
                break;
            case 5:
                this.enemy = new RangerAI(level);
                break;
            default:
                this.enemy = new ArcherAI(level);
        }
    }

    @Override
    public int getHP() {
        return this.enemy.getHP();
    }

    @Override
    public int getDamage() {
        return this.enemy.getDamage();
    }

    @Override
    public boolean isDead() {
        return this.enemy.getHP() == 0;
    }

    @Override
    public ArtificialIntelligence getEnemy() {
        return this.enemy;
    }

    /**
     * Set hp to enemy.
     *
     * @param hp : New hp
     */
    @Override
    public void setHP(int hp) {
        this.enemy.setHP(hp);
    }

    @Override
    public float getEvade() {
        return this.enemy.getEvasionRate();
    }
}
