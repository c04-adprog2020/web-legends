package com.c04.weblegends.service;

public interface BattleService {
    public void changeObject();

    public void changeStage();

    public int getLevel();

    public PlayerService getPlayer();

    public EnemyService getEnemy();

    public String playerAttack();

    public String enemyAttack();

    public String playerSpell();
}
