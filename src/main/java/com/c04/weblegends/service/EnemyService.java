package com.c04.weblegends.service;

import com.c04.weblegends.core.aicharacter.ArtificialIntelligence;

public interface EnemyService {
    public int getHP();

    public int getDamage();

    public boolean isDead();

    public void spawnEnemy(int level);

    public ArtificialIntelligence getEnemy();

    public void setHP(int hp);

    public float getEvade();

}
