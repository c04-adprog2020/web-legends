package com.c04.weblegends.service;

import com.c04.weblegends.core.character.CharComponent;
import com.c04.weblegends.core.character.CharFactory;
import com.c04.weblegends.core.character.PowerupFactory;
import com.c04.weblegends.core.character.Saber;
import com.c04.weblegends.core.item.Item;
import com.c04.weblegends.core.item.ItemFactory;
import com.c04.weblegends.core.weapon.EnhancementFactory;
import com.c04.weblegends.core.weapon.Sword;
import com.c04.weblegends.core.weapon.WeaponComponent;
import com.c04.weblegends.repo.PlayerRepo;
import java.util.ArrayList;
import org.springframework.stereotype.Service;

@Service
public class PlayerServiceImpl implements PlayerService {
    private CharComponent player;
    private ItemFactory itemFactory;
    private EnhancementFactory enhancementFactory;
    private PowerupFactory powerupFactory;
    private ArrayList<Item> itemList;
    private PlayerRepo playerRepo;

    /**
     * Initial setup (Could be changed in the future).
     */
    public PlayerServiceImpl() {
        player = new Saber("W");
        itemFactory = new ItemFactory(player);
        enhancementFactory = new EnhancementFactory();
        powerupFactory = new PowerupFactory();
        playerRepo = new PlayerRepo();
        this.player.setWeapon(new Sword(10, 3, "blade"));
        itemList = new ArrayList<Item>();
    }

    /**
     * Get item from random drop.
     *
     * @return Random item
     * @throws NullPointerException :Exception if there is null item
     */
    @Override
    public Item getItem() throws NullPointerException {
        try {
            Item item = itemFactory.createItem();
            itemList.add(item);
            return item;
        } catch (NullPointerException e) {
            throw new NullPointerException("Cant identify this kind of item");
        }
    }

    /**
     * Method to implement powerup on player.
     *
     * @return : Upgraded player
     */
    @Override
    public String doPowerUp() {
        this.player = powerupFactory.upgrade(this.player);
        return "Character successfully upgraded!";
    }

    /**
     * Method to implement powerup on player's weapon (Could be changed in the future).
     *
     * @return Enhanced weapon
     */
    @Override
    public String doEnhanceWeapon() throws NullPointerException {
        this.player.setWeapon(enhancementFactory.upgrade(this.player.getWeapon()));
        return "Weapon successfully upgraded!";
    }

    @Override
    public WeaponComponent getWeapon() {
        return this.player.getWeapon();
    }

    @Override
    public CharComponent getHero() {
        return this.player;
    }

    @Override
    public ArrayList<Item> getItems() {
        return this.itemList;
    }

    /**
     * Change player pointer on Item & ItemFactory instance.
     */
    @Override
    public void changeObject() {
        if (itemList.size() > 0) {
            for (Item item : itemList) {
                item.setPlayer(this.player);
            }
        }
        itemFactory.setCharacter(this.player);
    }

    /**
     * Consume the item.
     *
     * @param id : Index of item
     */
    @Override
    public void useItem(int id) {
        this.itemList.get(id).use();
        this.itemList.remove(id);
    }

    @Override
    public int getDamage() {
        return this.player.getWeapon().getAttack();
    }

    /**
     * Set player's HP.
     *
     * @param hp : HP that's wanted to be applied on player
     */
    @Override
    public void setHP(int hp) {
        this.player.setHP(hp);
    }

    @Override
    public int getHP() {
        return this.player.getHP();
    }

    /**
     * Hero creation based on class.
     *
     * @param name : Name of the hero
     * @param type : Class of the hero
     * @throws NullPointerException : Exception if there is null kind of className
     */
    @Override
    public void createHero(String name, String type) throws NullPointerException {
        try {
            CharFactory charFactory = playerRepo.getFactoryByName(type);
            this.player = charFactory.produce(name);
            itemFactory = new ItemFactory(this.player);
            itemList = new ArrayList<Item>();
        } catch (NullPointerException e) {
            //Do Nothing
        }
    }


    @Override
    public PlayerRepo getRepo() {
        return this.playerRepo;
    }

    @Override
    public boolean isGameOver() {
        return this.player.getHP() == 0;
    }

}
