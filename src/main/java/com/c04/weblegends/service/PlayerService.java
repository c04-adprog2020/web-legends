package com.c04.weblegends.service;

import com.c04.weblegends.core.character.CharComponent;
import com.c04.weblegends.core.item.Item;
import com.c04.weblegends.core.weapon.WeaponComponent;
import com.c04.weblegends.repo.PlayerRepo;
import java.util.ArrayList;

public interface PlayerService {
    public Item getItem();

    public CharComponent getHero();

    public WeaponComponent getWeapon();

    public ArrayList<Item> getItems();

    public void useItem(int id);

    public String doPowerUp();

    public String doEnhanceWeapon();

    public void changeObject();

    public int getDamage();

    public int getHP();

    public void setHP(int hp);

    public void createHero(String name, String type);

    public PlayerRepo getRepo();

    public boolean isGameOver();
}
