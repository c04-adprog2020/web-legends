package com.c04.weblegends.service;

import java.util.Random;

public class BattleServiceImpl implements BattleService {
    private int level;
    private PlayerService playerService;
    private EnemyService enemyService;

    /**
     * Constructor for Battle Service.
     *
     * @param playerService : Player Service
     */
    public BattleServiceImpl(PlayerService playerService) {
        this.level = 0;
        this.playerService = playerService;
        this.enemyService = new EnemyServiceImpl();
    }

    /**
     * Point to new player instance.
     */
    @Override
    public void changeObject() {
        this.playerService.changeObject();
    }

    /**
     * Change to new stage.
     */
    @Override
    public void changeStage() {
        this.level++;
        this.enemyService.spawnEnemy(this.level);
        this.changeObject();
    }

    @Override
    public int getLevel() {
        return this.level;
    }

    @Override
    public PlayerService getPlayer() {
        return this.playerService;
    }

    @Override
    public EnemyService getEnemy() {
        return this.enemyService;
    }

    /**
     * Implement player attack to enemy.
     *
     * @return battle log
     */
    @Override
    public String playerAttack() {
        this.playerService.getHero().setSpellCooldown(Math.max(0,this.playerService.getHero().getSpellCooldown() - 1));
        float chance = new Random().nextFloat();
            if (chance > this.enemyService.getEvade()) {
                this.enemyService.setHP(this.enemyService.getHP() - this.playerService.getDamage());
                return String.format("Player Turn : Player dealt %d damage to enemy!", this.playerService.getDamage());
            }return "Player Turn : Enemy successfully dodged the attack!";
    }


    /**
     * Implement player spell to enemy.
     *
     * @return battle log
     */
    public String playerSpell(){
        switch (this.playerService.getHero().getSpell().getName()) {
            case "slash":
                if (this.playerService.getHero().getMana() > this.playerService.getHero().getSpell().getMana() &&
                        this.playerService.getHero().getSpellCooldown() == 0) {
                    this.enemyService.setHP(this.enemyService.getHP() - 200);
                    this.playerService.getHero().setSpellCooldown(this.playerService.getHero().getSpell().getCooldown());
                    return String.format("Player Turn : Player use %s !", this.playerService.getHero().getSpell().getName());
                }else{
                    return "player can't use spell " + this.playerAttack();
                }
            case "heal":
                if (this.playerService.getHero().getMana() > this.playerService.getHero().getSpell().getMana() &&
                        this.playerService.getHero().getSpellCooldown() == 0) {
                    this.playerService.setHP(this.playerService.getHP() + 300);
                    this.playerService.getHero().setSpellCooldown(this.playerService.getHero().getSpell().getCooldown());
                    return String.format("Player Turn : Player use %s !", this.playerService.getHero().getSpell().getName());
                }else{
                    return "player can't use spell " + this.playerAttack();
                }
            case "multishot":
                if (this.playerService.getHero().getMana() > this.playerService.getHero().getSpell().getMana() &&
                        this.playerService.getHero().getSpellCooldown() == 0) {
                    this.enemyService.setHP(this.enemyService.getHP() - 600);
                    this.playerService.getHero().setSpellCooldown(this.playerService.getHero().getSpell().getCooldown());
                    return String.format("Player Turn : Player use %s !", this.playerService.getHero().getSpell().getName());
                }else{
                    return "player can't use spell " + this.playerAttack();
                }
            case "true strike":
                if (this.playerService.getHero().getMana() > this.playerService.getHero().getSpell().getMana() &&
                        this.playerService.getHero().getSpellCooldown() == 0) {
                    this.enemyService.setHP(this.enemyService.getHP() - 650);
                    this.playerService.getHero().setSpellCooldown(this.playerService.getHero().getSpell().getCooldown());
                    return String.format("Player Turn : Player use %s !", this.playerService.getHero().getSpell().getName());
                }else{
                    return "player can't use spell " + this.playerAttack();
                }
            case "backstab":
                if (this.playerService.getHero().getMana() > this.playerService.getHero().getSpell().getMana() &&
                        this.playerService.getHero().getSpellCooldown() == 0) {
                    this.enemyService.setHP(this.enemyService.getHP() - 1200);
                    this.playerService.getHero().setSpellCooldown(this.playerService.getHero().getSpell().getCooldown());
                    return String.format("Player Turn : Player use %s !", this.playerService.getHero().getSpell().getName());
                }else{
                    return "player can't use spell " + this.playerAttack();
                }
            case "nova":
                if (this.playerService.getHero().getMana() > this.playerService.getHero().getSpell().getMana() &&
                        this.playerService.getHero().getSpellCooldown() == 0) {
                    this.enemyService.setHP(this.enemyService.getHP() - 800);
                    this.playerService.getHero().setSpellCooldown(this.playerService.getHero().getSpell().getCooldown());
                    return String.format("Player Turn : Player use %s !", this.playerService.getHero().getSpell().getName());
                }else{
                    return "player can't use spell " + this.playerAttack();
                }
            case "snipe":
                if (this.playerService.getHero().getMana() > this.playerService.getHero().getSpell().getMana() &&
                        this.playerService.getHero().getSpellCooldown() == 0) {
                    this.enemyService.setHP(this.enemyService.getHP() - 1000);
                    this.playerService.getHero().setSpellCooldown(this.playerService.getHero().getSpell().getCooldown());
                    return String.format("Player Turn : Player use %s !", this.playerService.getHero().getSpell().getName());
                }else{
                    return "player can't use spell " + this.playerAttack();
                }
        }return "";
    }


    /**
     * Implement enemy attack to player.
     *
     * @return battle log
     */
    public String enemyAttack() {
        Random r = new Random();
        int randomValue;
        String result;
        this.enemyService.getEnemy().executeMove();
        switch (this.enemyService.getEnemy().getCurrentMove()) {
          case "Attack":
              this.enemyService.getEnemy().decreaseCooldown();
              randomValue = r.nextInt(100);
              if (randomValue < this.enemyService.getEnemy().getAccuracy()) {
                  int updatedHP = this.playerService.getHP() - this.enemyService.getDamage();
                  this.playerService.setHP(updatedHP);
                  result = "Enemy Turn : " + this.enemyService.getEnemy().getMoveDescription();
                  result += " Enemy dealt " + this.enemyService.getDamage();
                  result += " damage to player!";
                  return result;
              }
              return "Enemy Turn : Player successfully dodged the attack!";
          case "True Strike":
              this.enemyService.getEnemy().setSkillCooldown(4);
              this.playerService.setHP(this.playerService.getHP() - this.enemyService.getDamage());
              result = "Enemy Turn : " + this.enemyService.getEnemy().getMoveDescription();
              result += " Enemy dealt " + this.enemyService.getDamage();
              result += " damage to player!";
              return result;
          case "Multishot":
              this.enemyService.getEnemy().setSkillCooldown(4);
              int totalDamage = 0;
              for (int itr = 0; itr < 3; itr++) {
                  randomValue = r.nextInt(100);
                  if (randomValue < this.enemyService.getEnemy().getAccuracy()) {
                      totalDamage += this.enemyService.getDamage();
                  }
              }
              this.playerService.setHP(this.playerService.getHP() - totalDamage);
              result = "Enemy Turn : " + this.enemyService.getEnemy().getMoveDescription();
              result += " Enemy dealt " + totalDamage;
              result += " damage to player!";
              return result;
          case "Critical Strike":
              this.enemyService.getEnemy().setSkillCooldown(4);
              randomValue = r.nextInt(100);
              if (randomValue < this.enemyService.getEnemy().getAccuracy()) {
                  int updatedHP = this.playerService.getHP() - 2 * this.enemyService.getDamage();
                  this.playerService.setHP(updatedHP);
                  result = "Enemy Turn : " + this.enemyService.getEnemy().getMoveDescription();
                  result += " Enemy dealt " + 2 * this.enemyService.getDamage();
                  result += " damage to player!";
                  return result;
              }
              return "Enemy Turn : Player successfully dodged the attack!";
          case "Heal":
              this.enemyService.getEnemy().setSkillCooldown(4);
              this.enemyService.setHP(this.enemyService.getHP() + this.enemyService.getDamage());
              result = "Enemy Turn : " + this.enemyService.getEnemy().getMoveDescription();
              result += " Enemy heal " + this.enemyService.getDamage();
              result += " hit point!";
              return result;
          case "Pierce Attack":
              this.enemyService.getEnemy().setSkillCooldown(4);
              randomValue = r.nextInt(100);
              if (randomValue < this.enemyService.getEnemy().getAccuracy()) {
                  int updatedHP = this.playerService.getHP() - (20 + this.enemyService.getDamage());
                  this.playerService.setHP(updatedHP);
                  result = "Enemy Turn : " + this.enemyService.getEnemy().getMoveDescription();
                  result += " Enemy dealt " + (20 + this.enemyService.getDamage());
                  result += " damage to player!";
                  return result;
              }
              return "Enemy Turn : Player successfully dodged the attack!";
          case "Life Drain":
              this.enemyService.getEnemy().setSkillCooldown(4);
              this.playerService.setHP(this.playerService.getHP() - this.enemyService.getDamage());
              this.enemyService.setHP(this.enemyService.getHP() + this.enemyService.getDamage());
              result = "Enemy Turn : " + this.enemyService.getEnemy().getMoveDescription();
              result += " Enemy drain " + this.enemyService.getDamage();
              result += " hit point from player!";
              return result;
          default:
              return "Enemy Turn : Enemy doesn't make a move";
        }
    }
}
